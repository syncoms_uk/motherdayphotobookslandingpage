# MotherdayPhotobooksLandingPage
Landing page code for US photobooks promo landing page. 

CSS / JS / HTML

## HTML
mothersdaylandingpage.html - html for page

## CSS
Inc Media Queries for mobile / tablet etc
Core Styleheet - myStyle.css , style-uk.css
Override stylesheet - css/style11-US-ML.css (applies colour changes etc with css var)

## JS 
FlipClockUS-1.js - Controls timer for offer ending


## live page 
https://www.printerpix.com/mothers-day-photobooks-offer/
