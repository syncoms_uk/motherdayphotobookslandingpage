// Countdown-Timer for US
$(document).ready(function () {

    var date  = new Date(Date.UTC(2019, 4, 9, 8, 00, 00));
    var now   = new Date();
    var diff  = date.getTime()/1000 - now.getTime()/1000;
    if (diff < 0) {
      diff = 0;
    }
  
    
    var clock = $('#clock-big').FlipClock(diff, {
      clockFace: 'HourlyCounter',
      countdown: true,
      showSeconds: true
    });
    
  });